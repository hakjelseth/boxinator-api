﻿using AutoMapper;
using Boxinator_API.Models.Domain;
using Boxinator_API.Models.DTO;

namespace Boxinator_API.Profiles
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            CreateMap<Country, CountryDTO>().ReverseMap();
        }

    }
}
