﻿using AutoMapper;
using Boxinator_API.Models.Domain;
using Boxinator_API.Models.DTO;

namespace Boxinator_API.Profiles
{
    public class ShipmentProfile : Profile
    {
        public ShipmentProfile()
        {
            CreateMap<Shipment, ShipmentDTO>().ReverseMap();
        }

    }
}
