﻿using AutoMapper;
using Boxinator_API.Models.Domain;
using Boxinator_API.Models.DTO;

namespace Boxinator_API.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<Account, AccountDTO>().ReverseMap();
        }

    }
}
