using Boxinator_API.Models;
using Boxinator_API.Services;
using Boxinator_API.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;

namespace Boxinator_API
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        IssuerSigningKeyResolver = (token, securityToken, kid, parameters) =>
                        {
                            HttpClient client = new HttpClient();
                            string keyuri = Environment.GetEnvironmentVariable("APPSETTING_KeyURI");
                            HttpResponseMessage response = client.GetAsync(keyuri).Result;
                            string responseString = response.Content.ReadAsStringAsync().Result;
                            JsonWebKeySet keys = JsonConvert.DeserializeObject<JsonWebKeySet>(responseString);
                            return keys.Keys;
                        },

                        ValidIssuers = new List<string>
                        {
                            Environment.GetEnvironmentVariable("APPSETTING_IssuerURI")
                        },

                        ValidAudience = "account",
                    };
                });

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.WithOrigins("https://boxinatorclient.herokuapp.com",
                                            "http://localhost:3000")
                                                .AllowAnyHeader()
                                                .AllowAnyMethod();
                    });
            });
            var MailSettings = Configuration.GetSection("MailSettings");
            MailSettings["Mail"] = Environment.GetEnvironmentVariable("MailUser");
            MailSettings["Password"] = Environment.GetEnvironmentVariable("MailPassword");
            services.Configure<MailSettings>(MailSettings);
            services.AddTransient<IMailService, Services.MailService>();
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers();
            services.AddDbContext<BoxinatorDbContext>(options =>
                options.UseSqlServer(Environment.GetEnvironmentVariable("SQLCONNSTR_ConnectionString")));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Boxinator API",
                    Version = "v1",
                    Description = "API made for the boxinator case"
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Boxinator_API v1");
                c.RoutePrefix = string.Empty;
            });
            app.UseHttpsRedirection();

            app.UseRouting();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            app.UseCors();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseDeveloperExceptionPage();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
