﻿using Boxinator_API.Models;
using Boxinator_API.Models.Domain;
using Boxinator_API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Boxinator_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ShipmentsController : ControllerBase
    {
        private readonly BoxinatorDbContext _context;
        private readonly IMailService mailService;

        public ShipmentsController(BoxinatorDbContext context, IMailService mailService)
        {
            _context = context;
            this.mailService = mailService;
        }

        /// <summary>
        /// Get all relevant shipments.
        /// </summary>
        /// <returns>Task<ActionResult<IEnumerable<Shipment>>></returns>
        // GET: api/Shipments
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Shipment>>> GetShipments()
        {
            string id = User.FindFirst("sub").Value;
            bool realm_access = User.FindFirst("realm_access").Value.Contains("Administrator");


            if (realm_access)
            {
                //return await _context.Shipments.Where(x => (!x.Status.EndsWith("CANCELLED")) && (!x.Status.EndsWith("COMPLETED"))).ToListAsync();
                return await _context.Shipments.ToListAsync();
            }

            return await _context.Shipments.Where(x => (x.AccountID == id) && (!x.Status.EndsWith("CANCELLED")) && (!x.Status.EndsWith("COMPLETED"))).ToListAsync();
        }

        /// <summary>
        /// Get all complete shipments.
        /// </summary>
        /// <returns>Task<ActionResult<IEnumerable<Shipment>>></returns>
        // GET: api/Shipments/complete
        [Authorize]
        [HttpGet("complete")]
        public async Task<ActionResult<IEnumerable<Shipment>>> GetCompleteShipments()
        {
            string id = User.FindFirst("sub").Value;
            return await _context.Shipments.Where(x => (x.AccountID == id) && (x.Status.EndsWith("COMPLETED"))).ToListAsync();
        }

        /// <summary>
        /// Get all cancelled shipments.
        /// </summary>
        /// <returns>Task<ActionResult<IEnumerable<Shipment>>></returns>
        // GET: api/Shipments
        [Authorize]
        [HttpGet("cancelled")]
        public async Task<ActionResult<IEnumerable<Shipment>>> GetCancelledShipments()
        {
            string id = User.FindFirst("sub").Value;
            return await _context.Shipments.Where(x => (x.AccountID == id) && (x.Status.EndsWith("CANCELLED"))).ToListAsync();

            //return await _context.Shipments.ToListAsync();
        }

        /// <summary>
        /// Get shipment with specific shipment id.
        /// </summary>
        /// <param name="shipment_id"></param>
        /// <returns>Task<ActionResult<Shipment>></returns>
        // GET: api/Shipments/5
        [Authorize]
        [HttpGet("{shipment_id}")]
        public async Task<ActionResult<Shipment>> GetShipmentByID(int shipment_id)
        {
            string id = User.FindFirst("sub").Value;
            Shipment shipment = (await _context.Shipments.Where(x => (x.AccountID == id) && (x.Id == shipment_id)).ToListAsync())[0];

            if (shipment == null)
            {
                return NotFound();
            }

            return shipment;
        }

        /// <summary>
        /// Get shipments from specified user.
        /// </summary>
        /// <param name="customer_id"></param>
        /// <returns>Task<ActionResult<IEnumerable<Shipment>>></returns>
        // GET: api/Shipments/5
        [Authorize]
        [HttpGet("{customer_id}")]
        public async Task<ActionResult<IEnumerable<Shipment>>> GetShipmentByCustomer(string customer_id)
        {
            string id = User.FindFirst("sub").Value;
            List<Shipment> shipment = await _context.Shipments.Where(x => x.AccountID == customer_id).ToListAsync();

            return shipment;
        }

        /// <summary>
        /// Update shipment.
        /// </summary>
        /// <param name="shipment_id"></param>
        /// <param name="shipment"></param>
        /// <returns>Task<IActionResult></returns>
        // PUT: api/Shipments/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorize]
        [HttpPut("{shipment_id}")]
        public async Task<IActionResult> PutShipment(int shipment_id, Shipment shipment)
        {
            if (shipment_id != shipment.Id)
            {
                return BadRequest();
            }

            int weightKg = 1;
            if (shipment.Weight == "Humble")
            {
                weightKg = 2;
            }
            else if (shipment.Weight == "Deluxe")
            {
                weightKg = 5;
            }
            else if (shipment.Weight == "Premium")
            {
                weightKg = 8;
            }

            Shipment shipment1 = (await _context.Shipments.FindAsync(shipment_id));
            string lastStatus = shipment1.Status.Split(",").Last();
            if (lastStatus != shipment.Status)
                shipment.Status = shipment1.Status + "," + shipment.Status;
            _context.Entry(shipment1).State = EntityState.Detached;
            shipment.Country = (await _context.Countries.Where(x => x.Name == shipment.CountryName).ToListAsync())[0];
            shipment.Cost = 200 + (weightKg * shipment.Country.Multiplier);

            _context.Entry(shipment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShipmentExists(shipment_id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add new shipment.
        /// </summary>
        /// <param name="shipment"></param>
        /// <returns>Task<ActionResult<Shipment>></returns>
        // POST: api/Shipments
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<Shipment>> PostShipment(Shipment shipment)
        {
            int weightKg = 1;
            if (shipment.Weight == "Humble")
            {
                weightKg = 2;
            }
            else if (shipment.Weight == "Deluxe")
            {
                weightKg = 5;
            }
            else if (shipment.Weight == "Premium")
            {
                weightKg = 8;
            }

            string accountId = User.FindFirst("sub").Value;
            shipment.AccountID = accountId;
            shipment.Country = (await _context.Countries.Where(x => x.Name == shipment.CountryName).ToListAsync())[0];
            shipment.Cost = 200 + (weightKg * shipment.Country.Multiplier);
            _context.Shipments.Add(shipment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetShipmentByID", new { shipment_id = shipment.Id }, shipment);
        }

        /// <summary>
        /// Makes a guest shipment
        /// </summary>
        /// <param name="shipReg"></param>
        /// <returns>Task<ActionResult<Shipment>></returns>
        [HttpPost("guest")]
        public async Task<ActionResult<Shipment>> PostGuestShipment(ShipRegistration shipReg)
        {
            Shipment shipment = shipReg.Shipment;
            string regUrl = shipReg.RegUrl;
            int weightKg = 1;
            if (shipment.Weight == "Humble")
            {
                weightKg = 2;
            }
            else if (shipment.Weight == "Deluxe")
            {
                weightKg = 5;
            }
            else if (shipment.Weight == "Premium")
            {
                weightKg = 8;
            }

            MailRequest request = new MailRequest { ToEmail = shipment.AccountID, Subject = "Order receipt", Body = $"<h3>Boxinator receipt</h3>Sent to: {shipment.ReceiverName} <br> Weight class: {shipment.Weight} <br> Box color: {shipment.BoxColor} <br> Destination country: '{shipment.CountryName}' <br><br> <a href={regUrl}> Register an account to claim your order.</a> "  };

            try
            {
                await mailService.SendEmailAsync(request);
            }
            catch (Exception e) { }

            shipment.Country = (await _context.Countries.Where(x => x.Name == shipment.CountryName).ToListAsync())[0];
            shipment.Cost = 200 + (weightKg * shipment.Country.Multiplier);
            _context.Shipments.Add(shipment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetShipmentByID", new { shipment_id = shipment.Id }, shipment);
        }

        /// <summary>
        /// Delete shipment.
        /// </summary>
        /// <param name="shipment_id"></param>
        /// <returns>Task<IActionResult></returns>
        // DELETE: api/Shipments/5
        [Authorize]
        [HttpDelete("{shipment_id}")]
        public async Task<IActionResult> DeleteShipment(int shipment_id)
        {
            bool realm_access = User.FindFirst("realm_access").Value.Contains("Administrator");

            if (realm_access)
            {
                Shipment shipment = await _context.Shipments.FindAsync(shipment_id);
                if (shipment == null)
                {
                    return NotFound();
                }

                _context.Shipments.Remove(shipment);
                await _context.SaveChangesAsync();

                return NoContent();
            }

            return Unauthorized();
        }

        private bool ShipmentExists(int id)
        {
            return _context.Shipments.Any(e => e.Id == id);
        }
    }
    public class ShipRegistration
    {
        public Shipment Shipment { get; set; }
        public string RegUrl { get; set; }
    }

}
