﻿using Boxinator_API.Models;
using Boxinator_API.Models.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Boxinator_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CountriesController : ControllerBase
    {
        private readonly BoxinatorDbContext _context;

        public CountriesController(BoxinatorDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get a list of all the countries in the database.
        /// </summary>
        /// <returns>Task<ActionResult<IEnumerable<Country>>></returns>
        // GET: api/Countries
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Country>>> GetCountries()
        {
            return await _context.Countries.ToListAsync();
        }

        /// <summary>
        /// Get a country by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<Country>></returns>
        // GET: api/Countries/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<Country>> GetCountry(int id)
        {
        Country country = await _context.Countries.FindAsync(id);

        if (country == null)
        {
            return NotFound();
        }

        return country;
    }

        /// <summary>
        /// Update a country.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="country"></param>
        /// <returns>Task<IActionResult> PutCountry</returns>
        // PUT: api/Countries/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutCountry(int id, Country country)
        {
            if (id != country.Id)
            {
                return BadRequest();
            }

            _context.Entry(country).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CountryExists(country.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            await UpdateCosts(country.Name);

            return NoContent();
        }

        /// <summary>
        /// Add new country.
        /// </summary>
        /// <param name="country"></param>
        /// <returns>Task<ActionResult<Country>></returns>
        // POST: api/Countries
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<Country>> PostCountry(Country country)
        {
            _context.Countries.Add(country);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCountry", new { id = country.Id }, country);
        }

        /// <summary>
        /// Updates costs after you have changed a country
        /// </summary>
        /// <param name="CountryName"></param>
        /// <returns>Task<IActionResult></returns>
        public async Task<IActionResult> UpdateCosts(string CountryName)
        {
            List<Shipment> shipments = await _context.Shipments.Where(x => (!x.Status.EndsWith("CANCELLED")) && (!x.Status.EndsWith("COMPLETED")) && (x.CountryName == CountryName)).ToListAsync();
            Country EditedCountry = (await _context.Countries.Where(x => x.Name == CountryName).ToListAsync())[0];

            foreach (Shipment shipment in shipments)
            {
                int weightKg = 1;
                if (shipment.Weight == "Humble")
                {
                    weightKg = 2;
                }
                else if (shipment.Weight == "Deluxe")
                {
                    weightKg = 5;
                }
                else if (shipment.Weight == "Premium")
                {
                    weightKg = 8;
                }

                shipment.Cost = 200 + (weightKg * EditedCountry.Multiplier);
                _context.Entry(shipment).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }

            }

            return NoContent();
        }

        private bool CountryExists(int id)
        {
            return _context.Countries.Any(e => e.Id == id);
        }
    }
}
