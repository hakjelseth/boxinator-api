﻿using Boxinator_API.Models;
using Boxinator_API.Models.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Boxinator_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class AccountsController : ControllerBase
    {
        private readonly BoxinatorDbContext _context;

        public AccountsController(BoxinatorDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get account
        /// </summary>
        /// <returns>Task<ActionResult<Account>></returns>
        // GET: api/Accounts/{id}
        [Authorize]
        [HttpGet("account_id")]
        public async Task<ActionResult<Account>> GetAccount()
        {
            string user = User.FindFirst("sub").Value;
            Account account = await _context.Accounts.FindAsync(user);

            if (account == null)
            {
                return NotFound();
            }

            return account;
        }

        /// <summary>
        /// Login with account.
        /// </summary>
        /// <returns>Task<ActionResult<Account>></returns>
        //GET: api/login
        [Authorize]
        [HttpGet("login")]
        public async Task<ActionResult<Account>> Login()
        {
            string user = User.FindFirst("sub").Value;

            Account account = await _context.Accounts.FindAsync(user);

            if (account == null)
            {
                return await PostAccount();
            }

            return account;
        }

        /// <summary>
        /// Update an account.
        /// </summary>
        /// <param name="account_id"></param>
        /// <param name="account"></param>
        /// <returns>Task<IActionResult></returns>
        // PUT: api/Accounts/{id}
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{account_id}")]
        [Authorize]
        public async Task<IActionResult> PutAccount(string account_id, Account account)
        {
            if (account_id != account.Id)
            {
                return BadRequest();
            }

            _context.Entry(account).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(account_id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add new account.
        /// </summary>
        /// <returns>Task<ActionResult<Account>></returns>
        // POST: api/Accounts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<Account>> PostAccount()
        {
            string id = User.FindFirst("sub").Value;
            string firstName = User.FindFirst("given_name").Value;
            string lastName = User.FindFirst("family_name").Value;
            string email = User.FindFirst("email").Value;
            string country = User.FindFirst("country").Value;
            DateTime dob = DateTime.Parse(User.FindFirst("dob").Value);
            int mobile = Int32.Parse(User.FindFirst("mobile").Value);
            int postal = Int32.Parse(User.FindFirst("postal").Value);

            List<Account> guestAccounts = await _context.Accounts.Where(x => x.Id == email).ToListAsync();
            Account account = new() { Id = id, FirstName = firstName, LastName = lastName, Email = email, CountryOfResidence = country, ContactNumber = mobile, PostalCode = postal, DateOfBirth = dob };

            _context.Accounts.Add(account);
            await _context.SaveChangesAsync();

            if (guestAccounts.Count > 0)
            {

                List<Shipment> shipments = await _context.Shipments.Where(x => x.AccountID == email).ToListAsync();
                foreach (Shipment shipment in shipments)
                {
                    Shipment newShipment = new() { AccountID = id, ReceiverName = shipment.ReceiverName, Weight = shipment.Weight, BoxColor = shipment.BoxColor, CountryName = shipment.CountryName, Status = shipment.Status, Cost = shipment.Cost, Country = shipment.Country };
                    _context.Shipments.Remove(shipment);
                    await _context.SaveChangesAsync();
                    
                    _context.Shipments.Add(newShipment);
                    await _context.SaveChangesAsync();
                }

                _context.Accounts.Remove(guestAccounts[0]);
                await _context.SaveChangesAsync();

            }

             return CreatedAtAction("GetAccount", new { id = account.Id }, account);
            

        }

        /// <summary>
        /// Make a guest account
        /// </summary>
        /// <param name="account"></param>
        /// <returns>Task<ActionResult<Account>></returns>
        [HttpPost("guest")]
        public async Task<ActionResult<Account>> PostGuestAccount(Account account)
        {
            if (AccountExists(account.Email))
            {
                return CreatedAtAction("GetAccount", new { id = account.Id }, account); ;
            }

            if (AccountExistsEmail(account.Email))
            {
                return BadRequest();
            }
            _context.Accounts.Add(account);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAccount", new { id = account.Id }, account);
        }

        /// <summary>
        /// Delete account.
        /// </summary>
        /// <param name="account_id"></param>
        /// <returns>Task<IActionResult></returns>
        // DELETE: api/Accounts/{id}
        [Authorize]
        [HttpDelete("{account_id}")]
        public async Task<IActionResult> DeleteAccount(string account_id)
        {
            bool realm_access = User.FindFirst("realm_access").Value.Contains("Administrator");

            if (!realm_access)
                return Unauthorized();

            Account account = await _context.Accounts.FindAsync(account_id);
            if (account == null)
            {
                return NotFound();
            }

            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AccountExists(string id)
        {
            return _context.Accounts.Any(e => e.Id == id);
        }

        private bool AccountExistsEmail(string id)
        {
            return _context.Accounts.Any(e => e.Email == id);
        }
    }
}
