﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Boxinator_API.Models.Domain
{
    [Table("Account")]
    public class Account
    {
        public string Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CountryOfResidence { get; set; }
        public int PostalCode { get; set; }
        public int ContactNumber { get; set; }


    }
}
