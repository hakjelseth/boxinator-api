﻿using Boxinator_API.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Boxinator_API.Models.Domain
{
    [Table("Shipment")]
    public class Shipment
    {
        public int Id { get; set; }
        public string AccountID { get; set; }
        public Account Account { get; set; }
        [Required]
        [MaxLength(50)]
        public string ReceiverName { get; set; }
        [Required]
        public string Weight { get; set; }
        [Required]
        [MaxLength(50)]
        public string BoxColor { get; set; }
        [Required]
        public string CountryName { get; set; }
        public Country Country { get; set; }
        public string Status { get; set; } = ShipmentStatus.CREATED.ToString();
        public int Cost { get; set; }

    }
}
