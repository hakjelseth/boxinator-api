﻿using Boxinator_API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace Boxinator_API.Models
{
    public class BoxinatorDbContext : DbContext
    {
        public BoxinatorDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<Country> Countries { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Country>().HasData(new Country { Id = 1, Name = "Norway", Multiplier = 0 });

            modelBuilder.Entity<Country>().HasData(new Country { Id = 2, Name = "Sweden", Multiplier = 0 });

            modelBuilder.Entity<Country>().HasData(new Country { Id = 3, Name = "Denmark", Multiplier = 0 });

            modelBuilder.Entity<Country>().HasData(new Country { Id = 4, Name = "Germany", Multiplier = 5 });

        }
    }
}
