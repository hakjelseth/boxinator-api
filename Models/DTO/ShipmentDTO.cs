﻿namespace Boxinator_API.Models.DTO
{
    public class ShipmentDTO
    {
        public int Id { get; set; }
        public string AccountID { get; set; }
        public string ReceiverName { get; set; }
        public string Weight { get; set; }
        public string BoxColor { get; set; }
        public string CountryName { get; set; }
        public string Status { get; set; }
        public int Cost { get; set; }
    }
}
