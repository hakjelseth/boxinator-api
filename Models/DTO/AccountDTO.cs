﻿using System;

namespace Boxinator_API.Models.DTO
{
    public class AccountDTO
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CountryOfResidence { get; set; }
        public int PostalCode { get; set; }
        public int ContactNumber { get; set; }
    }
}
