﻿
using Boxinator_API.Models;
using System.Threading.Tasks;

namespace Boxinator_API.Services
{
    public interface IMailService
    {
        Task SendEmailAsync(MailRequest mailRequest);

    }
}
